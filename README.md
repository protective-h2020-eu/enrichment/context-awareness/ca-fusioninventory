PROTECTIVE  Context Awareness (CA) - FusionInventory to MAIR Bridge
========== 

This version of `ca-fusioninventory` is installed with `ca-mair` on an individual machine that supports docker. The two products are distributed as docker images, each stored in different docker registry locations on GitLab. A MongoDB image, located in DockerHub, is also installed on the same machine.

To install the products one must follow the instructions in the `README.md` of the `ca-mair` project located at `https://gitlab.com/protective-h2020-eu/enrichment/context-awareness/ca-mair.git`

Building this image
--------
In order to build this image 
- clone this repository
- checkout the appropriate branch, only the master branch is being delivered!
- on Linux/macOS run `./gradlew buildDocker` in a terminal
- on Windows run `gradlew.bat buildDocker` at the command prompt

In order to push this image to the GitLab registry
- on Linux/macOS run `./gradlew pushDocker` in a terminal
- on Windows run `gradlew.bat pushDocker` at the command prompt