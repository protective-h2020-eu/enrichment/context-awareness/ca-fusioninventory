package eu.h2020.protective.ca.fusioninventory;

import java.util.Optional;

import org.springframework.boot.info.BuildProperties;

import lombok.Data;

@Data
public class AboutProperties {

	private final String name;
	private final String group;
	private final String artifact;
	private final String version;
	private final String time;
	private final String description;

	public AboutProperties(Optional<BuildProperties> buildProperties) {
		if (buildProperties.isPresent()) {
			BuildProperties build = buildProperties.get();
			this.name = build.getName();
			this.group = build.getGroup();
			this.artifact = build.getArtifact();
			this.version = build.getVersion();
			this.time = build.get("formattedTime");
			this.description = build.get("description");
		} else {
			this.name = "";
			this.group = "";
			this.artifact = "";
			this.version = "";
			this.time = "";
			this.description = "Build Properties are not defined as not running gradle build";
		}
	}

}
