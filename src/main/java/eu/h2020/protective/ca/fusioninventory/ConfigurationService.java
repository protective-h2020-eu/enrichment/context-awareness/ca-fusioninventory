package eu.h2020.protective.ca.fusioninventory;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

import lombok.Data;

@Configuration
@ConfigurationProperties(prefix = "ca-fusioninventory")
@Data
public class ConfigurationService {

	private String caMairUrl;
	private String defaultCdg;
	
}
