package eu.h2020.protective.ca.fusioninventory.xml;

import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.cfg.MapperConfig;
import com.fasterxml.jackson.databind.introspect.AnnotatedField;
import com.fasterxml.jackson.databind.introspect.AnnotatedMethod;

public class SameCaseNamingStrategy extends PropertyNamingStrategy {

	private static final long serialVersionUID = 8447422037966566461L;

	@Override
	public String nameForField(MapperConfig<?> config, AnnotatedField field, String defaultName) {
		String name = field.getName();
		return name;
	}

	@Override
	public String nameForGetterMethod(MapperConfig<?> config, AnnotatedMethod method,
			String defaultName) {
		String name = method.getName().substring(3);
		return name;
	}

	@Override
	public String nameForSetterMethod(MapperConfig<?> config, AnnotatedMethod method,
			String defaultName) {
		String name = method.getName().substring(3);
		return name;
	}
	
}