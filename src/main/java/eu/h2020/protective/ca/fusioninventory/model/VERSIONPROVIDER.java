package eu.h2020.protective.ca.fusioninventory.model;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper=true)
public class VERSIONPROVIDER extends FusionInventory {

	@ApiModelProperty(required = false, value = "")
	private String NAME;

	@ApiModelProperty(required = false, value = "")
	private String VERSION;

	@ApiModelProperty(required = false, value = "")
	private String COMMENTS;

	@ApiModelProperty(required = false, value = "")
	private String PERL_EXE;

	@ApiModelProperty(required = false, value = "")
	private String PERL_VERSION;

	@ApiModelProperty(required = false, value = "")
	private String PERL_ARGS;

	@ApiModelProperty(required = false, value = "")
	private String PROGRAM;

	@ApiModelProperty(required = false, value = "")
	private String PERL_CONFIG;

	@ApiModelProperty(required = false, value = "")
	private String PERL_INC;

	@ApiModelProperty(required = false, value = "")
	private String PERL_MODULE;

}
