package eu.h2020.protective.ca.fusioninventory.filter;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.util.zip.InflaterInputStream;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletInputStream;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletRequestWrapper;

import org.springframework.stereotype.Component;

import lombok.extern.slf4j.XSlf4j;

@Component
@XSlf4j
public class FiDecompressZlibFilter implements Filter {

	@Override
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
			throws IOException, ServletException {

		HttpServletRequest httpServletRequest = null;
		if (request instanceof HttpServletRequest) {
			httpServletRequest = (HttpServletRequest) request;
		}

		String userAgent = httpServletRequest.getHeader("user-agent");
		String contentType = httpServletRequest.getContentType();
		String transferEncoding = httpServletRequest.getHeader("te");

		if (userAgent != null) {
			if (userAgent.startsWith("FusionInventory-Agent") || userAgent.startsWith("FusionInventory-Injector")) {
				if (contentType != null) {
					if (contentType.equalsIgnoreCase(XCompressZlibMediaType.APPLICATION_X_COMPRESS_ZLIB_VALUE)
							|| contentType.equalsIgnoreCase(XCompressMediaType.APPLICATION_X_COMPRESS_VALUE)) {
						if (transferEncoding != null // && transferEncoding.contains("gzip")
								&& transferEncoding.contains("deflate")) {
							try {
								final InputStream decompressStream = new InflaterInputStream(
										httpServletRequest.getInputStream());

								httpServletRequest = new HttpServletRequestWrapper(httpServletRequest) {

									@Override
									public ServletInputStream getInputStream() throws IOException {
										return new DecompressZlibServletInputStream(decompressStream);
									}

									@Override
									public BufferedReader getReader() throws IOException {
										return new BufferedReader(
												new InputStreamReader(decompressStream, StandardCharsets.UTF_8));
									}
								};
							} catch (IOException e) {
								log.error("error while handling the request", e);
							}
						}
					}
				}
			}
		}

		chain.doFilter(httpServletRequest, response);
	}

	@Override
	public void destroy() {
	}

	@Override
	public void init(FilterConfig arg0) throws ServletException {
	}

}
