package eu.h2020.protective.ca.fusioninventory.filter;

import java.nio.charset.StandardCharsets;

import org.springframework.http.MediaType;

public class XCompressZlibMediaType extends MediaType {

	private static final long serialVersionUID = 2708426405828352570L;

	private static final String TYPE_VALUE = "application";
	private static final String SUBTYPE_VALUE = "x-compress-zlib";

	public static final MediaType APPLICATION_X_COMPRESS_ZLIB = new XCompressZlibMediaType();
	public static final String APPLICATION_X_COMPRESS_ZLIB_VALUE = TYPE_VALUE + "/" + SUBTYPE_VALUE;

	private XCompressZlibMediaType() {
		super(TYPE_VALUE, SUBTYPE_VALUE, StandardCharsets.UTF_8);
	}

}
