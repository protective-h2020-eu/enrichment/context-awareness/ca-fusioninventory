package eu.h2020.protective.ca.fusioninventory.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper=true)
@ApiModel
public class USERS extends FusionInventory {

	@ApiModelProperty(required = false, value = "Maps to User.role")
    private String LOGIN;

	@ApiModelProperty(required = false, value = "Ignore")
	private String DOMAIN;

}
