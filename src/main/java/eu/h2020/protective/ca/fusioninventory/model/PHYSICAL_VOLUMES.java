package eu.h2020.protective.ca.fusioninventory.model;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper=true)
public class PHYSICAL_VOLUMES extends FusionInventory {

	@ApiModelProperty(required = false, value = "")
	private String DEVICE;

	@ApiModelProperty(required = false, value = "")
	private String PV_PE_COUNT;

	@ApiModelProperty(required = false, value = "")
	private String PV_UUID;

	@ApiModelProperty(required = false, value = "")
	private String FORMAT;

	@ApiModelProperty(required = false, value = "")
	private String ATTR;

	@ApiModelProperty(required = false, value = "")
	private String SIZE;

	@ApiModelProperty(required = false, value = "")
	private String FREE;

	@ApiModelProperty(required = false, value = "")
	private String PE_SIZE;

	@ApiModelProperty(required = false, value = "")
	private String VG_UUID;

}
