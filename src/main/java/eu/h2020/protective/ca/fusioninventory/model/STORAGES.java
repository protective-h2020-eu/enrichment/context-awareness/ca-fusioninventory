package eu.h2020.protective.ca.fusioninventory.model;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper=true)
public class STORAGES extends FusionInventory {

	@ApiModelProperty(required = false, value = "")
	private String DESCRIPTION;

	@ApiModelProperty(required = false, value = "")
	private String DISKSIZE;

	@ApiModelProperty(required = false, value = "")
	private String INTERFACE;

	@ApiModelProperty(required = false, value = "")
	private String MANUFACTURER;

	@ApiModelProperty(required = false, value = "")
	private String MODEL;

	@ApiModelProperty(required = false, value = "")
	private String NAME;

	@ApiModelProperty(required = false, value = "")
	private String TYPE;

	@ApiModelProperty(required = false, value = "")
	private String SERIAL;

	@ApiModelProperty(required = false, value = "")
	private String SERIALNUMBER;

	@ApiModelProperty(required = false, value = "")
	private String FIRMWARE;
	
	@ApiModelProperty(required = false, value = "")
	private String SCSI_COID;
	
	@ApiModelProperty(required = false, value = "")
	private String SCSI_CHID;
	
	@ApiModelProperty(required = false, value = "")
	private String SCSI_UNID;
	
	@ApiModelProperty(required = false, value = "")
	private String SCSI_LUN;
	
	@ApiModelProperty(required = false, value = "")
	private String WWN;
	
}
