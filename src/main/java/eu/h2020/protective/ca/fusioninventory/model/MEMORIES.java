package eu.h2020.protective.ca.fusioninventory.model;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper=true)
public class MEMORIES extends FusionInventory {

	@ApiModelProperty(required = false, value = "")
	private String CAPACITY;

	@ApiModelProperty(required = false, value = "")
	private String CAPTION;

	@ApiModelProperty(required = false, value = "")
	private String FORMFACTOR;

	@ApiModelProperty(required = false, value = "")
	private String REMOVABLE;
	
	@ApiModelProperty(required = false, value = "")
	private String PURPOSE;
	
	@ApiModelProperty(required = false, value = "")
	private String SPEED;

	@ApiModelProperty(required = false, value = "")
	private String SERIALNUMBER;

	@ApiModelProperty(required = false, value = "")
	private String TYPE;

	@ApiModelProperty(required = false, value = "")
	private String DESCRIPTION;

	@ApiModelProperty(required = false, value = "")
	private String NUMSLOTS;

	@ApiModelProperty(required = false, value = "")
	private String MEMORYCORRECTION;
	
	@ApiModelProperty(required = false, value = "")
	private String MANUFACTURER;
	
}
