package eu.h2020.protective.ca.fusioninventory.model;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper=true)
public class SOUNDS extends FusionInventory {

	@ApiModelProperty(required = false, value = "")
	private String CAPTION;

	@ApiModelProperty(required = false, value = "")
	private String DESCRIPTION;

	@ApiModelProperty(required = false, value = "")
	private String MANUFACTURER;

	@ApiModelProperty(required = false, value = "")
	private String NAME;

}
