package eu.h2020.protective.ca.fusioninventory.model;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper=true)
public class RUDDER extends FusionInventory {

	@ApiModelProperty(required = false, value = "")
	private String AGENT;

	@ApiModelProperty(required = false, value = "")
	private String UUID;

	@ApiModelProperty(required = false, value = "")
	private String HOSTNAME;

	@ApiModelProperty(required = false, value = "")
	private String SERVER_ROLES;

	@ApiModelProperty(required = false, value = "")
	private String AGENT_CAPABILITIES;

}
