package eu.h2020.protective.ca.fusioninventory;

import java.io.InputStream;
import java.util.Arrays;
import java.util.List;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.dataformat.xml.JacksonXmlModule;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;

import eu.h2020.protective.ca.fusioninventory.model.CONTENT;
import eu.h2020.protective.ca.fusioninventory.model.NETWORKS;
import eu.h2020.protective.ca.fusioninventory.model.REQUEST;
import eu.h2020.protective.ca.fusioninventory.model.SOFTWARES;
import eu.h2020.protective.ca.fusioninventory.model.VIRTUALMACHINES;
import eu.h2020.protective.ca.mair.api.model.core.Edge;
import eu.h2020.protective.ca.mair.api.model.core.Graph;
import eu.h2020.protective.ca.mair.api.model.core.Vertex;
import eu.h2020.protective.ca.mair.api.model2.assets.OperatingSystem;
import eu.h2020.protective.ca.mair.api.model2.assets.Software;
import eu.h2020.protective.ca.mair.api.model2.edges.RunsOn;
import eu.h2020.protective.ca.mair.api.model2.edges.Uses;
import eu.h2020.protective.ca.mair.api.model2.nodes.Node;
import eu.h2020.protective.ca.mair.api.model2.nodes.VirtualMachine;
import eu.h2020.protective.ca.mair.api.model2.other.User;
import lombok.Setter;
import lombok.extern.slf4j.XSlf4j;

@Service
@XSlf4j
public class SyncMair {

	@Autowired
	private ConfigurationService config;

	@Value("classpath:inventory.xml")
	private Resource inventoryXml; // TODO move to test code

	@Setter
	private boolean strict = false;

	private XmlMapper xmlMapper;

	@PostConstruct
	private void initXmlMapper() {
		JacksonXmlModule module = new JacksonXmlModule();
		module.setDefaultUseWrapper(false);
		xmlMapper = new XmlMapper(module);
		xmlMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, strict);
		log.info("XmlMapper FAIL_ON_UNKNOWN_PROPERTIES = {}", strict);
	}

	public boolean isPrologXml(REQUEST request) throws Exception {
		return request.getQUERY().equals("PROLOG");
	}

	public boolean isInventoryXml(REQUEST request) throws Exception {
		return request.getQUERY().equals("INVENTORY");
	}

	public REQUEST getRequestFromXml() throws Exception { // TODO move to test code
		InputStream xmlStream = inventoryXml.getInputStream();
		return getRequestFromXml(xmlStream);
	}

	public REQUEST getRequestFromXml(InputStream xmlStream) throws Exception {
		REQUEST request = xmlMapper.readValue(xmlStream, REQUEST.class);
		return request;
	}

	public REQUEST getRequestFromXml(String xmlString) throws Exception {
		REQUEST request = xmlMapper.readValue(xmlString, REQUEST.class);
		return request;
	}

	public Graph getGraphFromDeviceXml(REQUEST request) throws Exception {
		Graph graph = new Graph();

		CONTENT content = request.getCONTENT();

		// TODO add more network info, e.g. 200, 201. (currently only adding IPs)
		Node device = new Node();
		device.setNodeId(request.getDEVICEID());
		device.setDns(content.getHARDWARE().getDNS());
		if (!isNullOrEmpty(content.getHARDWARE().getIPADDR())) {
			List<String> ipAddresses = Arrays.asList(content.getHARDWARE().getIPADDR().split("/"));
			device.getIpAddresses().addAll(ipAddresses);
		}
		device.setModel(content.getBIOS().getSMODEL());
		device.setName(content.getHARDWARE().getNAME());
		for (NETWORKS network : content.getNETWORKS()) {
			if (isNullOrEmpty(device.getMacAddress())) { // Always take first address as primary.
				if ((!isNullOrEmpty(network.getIPADDRESS()) && device.getIpAddresses().contains(network.getIPADDRESS()))
						|| (!isNullOrEmpty(network.getIPADDRESS6())
								&& device.getIpAddresses().contains(network.getIPADDRESS6()))) {
					device.setDefaultGateway(network.getIPGATEWAY());
					device.setDescription(network.getDESCRIPTION());
					device.setIpGateway(network.getIPGATEWAY());
					device.setIpV4Subnet(network.getIPSUBNET());
					device.setIpV4SubnetMask(network.getIPMASK());
					device.setIpV6Subnet(network.getIPSUBNET6());
					device.setIpV6SubnetMask(network.getIPMASK6());
					device.setMacAddress(network.getMACADDR());
				}
			}
			if (!isNullOrEmpty(network.getIPADDRESS())) {
				device.getIpV4Addresses().add(network.getIPADDRESS());
			}
			if (!isNullOrEmpty(network.getIPADDRESS6())) {
				device.getIpV6Addresses().add(network.getIPADDRESS6());
			}
		}
		device.setKey(content.getHARDWARE().getUUID());

		graph.addVertex(device);

		User deviceUser = new User();
		deviceUser.setName(content.getHARDWARE().getUSERID());
		deviceUser.setKey(String.format("%s-USER-%s", device.getKey(), deviceUser.getName()));

		graph.addVertex(deviceUser);

		Uses usesDevice = new Uses(deviceUser, device);
		addEdge(graph, deviceUser, device, usesDevice);

		OperatingSystem os = new OperatingSystem();
//		os.setName(content.getOPERATINGSYSTEM().getFULL_NAME());
//		os.setProduct(content.getOPERATINGSYSTEM().getFULL_NAME());
//		os.setVendor("Unknown Operating System Vendor!"); // FIXME Get vendor somewhere!
//		os.setVersion(content.getOPERATINGSYSTEM().getVERSION());
//		os.setKey(String.format("%s-%s-%s", device.getKey(), os.getName(), os.getVersion()));
		if (content.getOPERATINGSYSTEM() != null) {
			os.setName(content.getOPERATINGSYSTEM().getFULL_NAME());
			os.setProduct(content.getOPERATINGSYSTEM().getFULL_NAME());
			os.setVendor("Unknown Operating System Vendor!"); // FIXME Get vendor somewhere!
			os.setVersion(content.getOPERATINGSYSTEM().getVERSION());
			os.setKey(String.format("%s-%s-%s", device.getKey(), os.getName(), os.getVersion()));
		} else if (content.getOPERATINGSYSTEM() == null) {
			os.setName(content.getHARDWARE().getOSNAME());
			os.setProduct(content.getHARDWARE().getOSNAME());
			os.setVendor("Unknown Operating System Vendor!");
			os.setVersion(content.getHARDWARE().getOSVERSION());
			os.setKey(String.format("%s-%s-%s", device.getKey(), os.getName(), os.getVersion()));
		} else {
			os.setName("UNKNOWN");
			os.setProduct("UNKNOWN");
			os.setVendor("Unknown Operating System Vendor!");
			os.setVersion("UNKNOWN");
			os.setKey(String.format("%s-%s-%s", device.getKey(), os.getName(), os.getVersion()));
		}
		

		graph.addVertex(os);
		RunsOn runsOnDevice = new RunsOn(os, device);
		addEdge(graph, os, device, runsOnDevice);

		Uses usesOS = new Uses(deviceUser, os);
		addEdge(graph, deviceUser, os, usesOS);

		for (SOFTWARES software : content.getSOFTWARES()) {
			Software app = new Software();
			app.setName(software.getNAME());
			app.setProduct(software.getNAME());
			app.setVendor(software.getPUBLISHER());
			app.setVersion(software.getVERSION());
			app.setKey(String.format("%s-%s-%s", os.getKey(), app.getName(), app.getVersion()));

			graph.addVertex(app);
			Uses runsOnOS = new Uses(app, os);
			addEdge(graph, app, os, runsOnOS);

			if (software.getUSERID() != null) {
				User appUser = new User();
				appUser.setName(software.getUSERID());
				appUser.setKey(String.format("%s-USER-%s", app.getKey(), appUser.getName()));

				graph.addVertex(appUser);

				Uses usesApp = new Uses(appUser, app);
				addEdge(graph, appUser, app, usesApp);
			}
		}

		for (VIRTUALMACHINES vm : content.getVIRTUALMACHINES()) {
			VirtualMachine computer = new VirtualMachine();
			computer.setMacAddress(vm.getMAC());
			computer.setName(vm.getNAME());
			computer.setDeviceType(vm.getVMTYPE()); // TODO Should be "Virtual"?
			computer.setKey(String.format("%s-VM-%s", os.getKey(), vm.getUUID()));

			graph.addVertex(computer);
			RunsOn runsOnOS = new RunsOn(computer, os);
			addEdge(graph, computer, os, runsOnOS);
		}

		return graph;
	}

	private void addEdge(Graph graph, Vertex fromVertex, Vertex toVertex, Edge edge) {
		String cdg = config.getDefaultCdg();
		if (cdg == null || cdg.trim().isEmpty()) {
			cdg = Edge.OTHER_EDGES_CDG;
		}
		edge.setFromCdg(cdg);
		edge.setToCdg(cdg);
		graph.addEdge(fromVertex, toVertex, edge);
	}

	private boolean isNullOrEmpty(String string) {
		if (string == null) {
			return true;
		}
		if (string.trim().isEmpty()) {
			return true;
		}
		return false;
	}

}
