package eu.h2020.protective.ca.fusioninventory.model;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper=true)
public class BIOS extends FusionInventory {

	@ApiModelProperty(required = false, value = "")
	private String SMODEL;
	
	@ApiModelProperty(required = false, value = "")
	private String SMANUFACTURER;

	@ApiModelProperty(required = false, value = "")
	private String SSN;

	@ApiModelProperty(required = false, value = "")
	private String BDATE;

	@ApiModelProperty(required = false, value = "")
	private String BVERSION;

	@ApiModelProperty(required = false, value = "")
	private String BMANUFACTURER;

	@ApiModelProperty(required = false, value = "")
	private String MMANUFACTURER;

	@ApiModelProperty(required = false, value = "")
	private String MSN;

	@ApiModelProperty(required = false, value = "")
	private String MMODEL;

	@ApiModelProperty(required = false, value = "")
	private String ASSETTAG;

	@ApiModelProperty(required = false, value = "")
	private String ENCLOSURESERIAL;

	@ApiModelProperty(required = false, value = "")
	private String BIOSSERIAL;

	@ApiModelProperty(required = false, value = "")
	private String TYPE;
	
	@ApiModelProperty(required = false, value = "")
	private String SKUNUMBER;

}
