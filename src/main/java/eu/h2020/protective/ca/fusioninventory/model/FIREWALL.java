package eu.h2020.protective.ca.fusioninventory.model;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper=true)
public class FIREWALL extends FusionInventory {

	@ApiModelProperty(required = false, value = "")
	private String PROFILE;

	@ApiModelProperty(required = false, value = "")
	private String STATUS;

	@ApiModelProperty(required = false, value = "")
	private String DESCRIPTION;

	@ApiModelProperty(required = false, value = "")
	private String IPADDRESS;

	@ApiModelProperty(required = false, value = "")
	private String IPADDRESS6;

}
