package eu.h2020.protective.ca.fusioninventory.model;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper=true)
public class REPLY extends FusionInventory {

	private String RESPONSE;
	private Integer PROLOG_FREQ;
	
}
