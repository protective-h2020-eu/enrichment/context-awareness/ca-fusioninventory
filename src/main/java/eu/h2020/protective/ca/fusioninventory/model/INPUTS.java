package eu.h2020.protective.ca.fusioninventory.model;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper=true)
public class INPUTS extends FusionInventory {

	@ApiModelProperty(required = false, value = "")
	private String NAME;

	@ApiModelProperty(required = false, value = "")
	private String MANUFACTURER;

	@ApiModelProperty(required = false, value = "")
	private String CAPTION;

	@ApiModelProperty(required = false, value = "")
	private String DESCRIPTION;

	@ApiModelProperty(required = false, value = "")
	private String INTERFACE;

	@ApiModelProperty(required = false, value = "")
	private String LAYOUT;

	@ApiModelProperty(required = false, value = "")
	private String POINTINGTYPE;

	@ApiModelProperty(required = false, value = "")
	private String TYPE;

}
