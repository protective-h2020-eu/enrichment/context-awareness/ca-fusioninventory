package eu.h2020.protective.ca.fusioninventory.model;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper=true)
public class USBDEVICES extends FusionInventory {

	@ApiModelProperty(required = false, value = "")
	private String VENDORID;

	@ApiModelProperty(required = false, value = "")
	private String PRODUCTID;

	@ApiModelProperty(required = false, value = "")
	private String MANUFACTURER;

	@ApiModelProperty(required = false, value = "")
	private String CAPTION;

	@ApiModelProperty(required = false, value = "")
	private String SERIAL;

	@ApiModelProperty(required = false, value = "")
	private String CLASS;

	@ApiModelProperty(required = false, value = "")
	private String SUBCLASS;

	@ApiModelProperty(required = false, value = "")
	private String NAME;

}
