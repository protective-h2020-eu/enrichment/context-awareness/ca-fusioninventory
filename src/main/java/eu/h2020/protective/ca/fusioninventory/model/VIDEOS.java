package eu.h2020.protective.ca.fusioninventory.model;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper=true)
public class VIDEOS extends FusionInventory {

	@ApiModelProperty(required = false, value = "")
	private String CHIPSET;

	@ApiModelProperty(required = false, value = "")
	private String MEMORY;

	@ApiModelProperty(required = false, value = "")
	private String NAME;

	@ApiModelProperty(required = false, value = "")
	private String RESOLUTION;

	@ApiModelProperty(required = false, value = "")
	private String PCISLOT;

	@ApiModelProperty(required = false, value = "")
	private String PCIID;

}
