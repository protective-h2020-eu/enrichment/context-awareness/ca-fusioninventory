package eu.h2020.protective.ca.fusioninventory.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper=true)
@ApiModel
public class NETWORKS extends FusionInventory {

	@ApiModelProperty(required = false, value = "Maps to Network.name")
	private String DESCRIPTION;

	@ApiModelProperty(required = false, value = "Ignore")
	private String MANUFACTURER;

	@ApiModelProperty(required = false, value = "Ignore")
	private String MODEL;

	@ApiModelProperty(required = false, value = "Ignore")
	private String MANAGEMENT;

	@ApiModelProperty(required = false, value = "Ignore")
	private String TYPE;

	@ApiModelProperty(required = false, value = "Ignore")
	private String VIRTUALDEV;

	@ApiModelProperty(required = false, value = "Maps to Network.macAddress")
	private String MACADDR;

	@ApiModelProperty(required = false, value = "Ignore")
	private String WWN;

	@ApiModelProperty(required = false, value = "Ignore")
	private String DRIVER;

	@ApiModelProperty(required = false, value = "Ignore")
	private String FIRMWARE;

	@ApiModelProperty(required = false, value = "Ignore")
	private String PCIID;

	@ApiModelProperty(required = false, value = "Ignore")
	private String PCISLOT;

	@ApiModelProperty(required = false, value = "Ignore")
	private String PNPDEVICEID;

	@ApiModelProperty(required = false, value = "Ignore")
	private String MTU;

	@ApiModelProperty(required = false, value = "Ignore")
	private String SPEED;

    @ApiModelProperty(required = false, value = "Ignore: e.g. 'up' (network card or network?)")
	private String STATUS;

    @ApiModelProperty(required = false, value = "Ignore")
	private String SLAVES;

    @ApiModelProperty(required = false, value = "Ignore")
	private String BASE;

	@ApiModelProperty(required = false, value = "Maps to Network.ipv4Address")
	private String IPADDRESS;
	
	@ApiModelProperty(required = false, value = "Maps to Network.ipv4Subnet")
	private String IPSUBNET;

	@ApiModelProperty(required = false, value = "Maps to Network.ipv4SubnetMask")
	private String IPMASK;

    @ApiModelProperty(required = false, value = "Ignore")
	private String IPDHCP;

	@ApiModelProperty(required = false, value = "Ignore")
	private String IPGATEWAY;

	@ApiModelProperty(required = false, value = "Maps to Network.ipv6Address")
	private String IPADDRESS6;

	@ApiModelProperty(required = false, value = "Maps to Network.ipv6Subnet")
	private String IPSUBNET6;

	@ApiModelProperty(required = false, value = "Maps to Network.ipv6SubnetMask")
	private String IPMASK6;

	@ApiModelProperty(required = false, value = "Ignore")
	private String WIFI_BSSID;

	@ApiModelProperty(required = false, value = "Ignore")
	private String WIFI_SSID;

	@ApiModelProperty(required = false, value = "Ignore")
	private String WIFI_MODE;

	@ApiModelProperty(required = false, value = "Ignore")
	private String WIFI_VERSION;

}
