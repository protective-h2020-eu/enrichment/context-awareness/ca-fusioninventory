package eu.h2020.protective.ca.fusioninventory.model;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper=true)
public class REMOTE_MGMT extends FusionInventory {

	@ApiModelProperty(required = false, value = "")
	private String ID;

	@ApiModelProperty(required = false, value = "")
	private String TYPE;

}
