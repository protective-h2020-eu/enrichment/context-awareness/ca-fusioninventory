package eu.h2020.protective.ca.fusioninventory.model;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper=true)
public class BATTERIES extends FusionInventory {

	@ApiModelProperty(required = false, value = "")
	private String CAPACITY;

	@ApiModelProperty(required = false, value = "")
	private String CHEMISTRY;

	@ApiModelProperty(required = false, value = "")
	private String DATE;

	@ApiModelProperty(required = false, value = "")
	private String NAME;

    @ApiModelProperty(required = false, value = "")
	private String SERIAL;

	@ApiModelProperty(required = false, value = "")
	private String MANUFACTURER;

	@ApiModelProperty(required = false, value = "")
	private String VOLTAGE;

}
