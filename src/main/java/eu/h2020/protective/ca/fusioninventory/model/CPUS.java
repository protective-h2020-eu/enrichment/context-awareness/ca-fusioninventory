package eu.h2020.protective.ca.fusioninventory.model;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper=true)
public class CPUS extends FusionInventory {

	@ApiModelProperty(required = false, value = "")
	private String CACHE;

	@ApiModelProperty(required = false, value = "")
	private String CORE;

	@ApiModelProperty(required = false, value = "")
	private String DESCRIPTION;

	@ApiModelProperty(required = false, value = "")
	private String MANUFACTURER;

	@ApiModelProperty(required = false, value = "")
	private String NAME;

	@ApiModelProperty(required = false, value = "")
	private String THREAD;

	@ApiModelProperty(required = false, value = "")
	private String SERIAL;

	@ApiModelProperty(required = false, value = "")
	private String STEPPING;

    @ApiModelProperty(required = false, value = "")
	private String FAMILYNAME;

	@ApiModelProperty(required = false, value = "")
	private String FAMILYNUMBER;

	@ApiModelProperty(required = false, value = "")
	private String MODEL;

	@ApiModelProperty(required = false, value = "")
	private String SPEED;

	@ApiModelProperty(required = false, value = "")
	private String ID;
	
	@ApiModelProperty(required = false, value = "")
	private String EXTERNAL_CLOCK;

	@ApiModelProperty(required = false, value = "")
	private String ARCH;

	@ApiModelProperty(required = false, value = "")
	private String CORECOUNT;
   
}
