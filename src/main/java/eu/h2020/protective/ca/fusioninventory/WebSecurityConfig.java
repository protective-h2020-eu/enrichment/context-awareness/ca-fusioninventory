//package eu.h2020.protective.ca.fusioninventory;
//
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.context.annotation.Configuration;
//import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
//import org.springframework.security.config.annotation.web.builders.HttpSecurity;
//import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
//import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
//
//@Configuration
//@EnableWebSecurity
//public class WebSecurityConfig extends WebSecurityConfigurerAdapter {
//
//	@Override
//	protected void configure(HttpSecurity http) throws Exception {
//		http.authorizeRequests().anyRequest().fullyAuthenticated().and().httpBasic().and().csrf()
//				.disable(); // TODO add role somewhere, not needed at the moment as only one role
//	}
//
//	@Autowired // FIXME Put passwords somewhere else.
//	public void configureGlobal(AuthenticationManagerBuilder auth) throws Exception {
//		auth.inMemoryAuthentication().withUser("agent").password("Doctor.has.12").roles("AGENT")
//				.and().withUser("admin").password("doctor112911999").roles("ACTUATOR");
//	}
//
//}
