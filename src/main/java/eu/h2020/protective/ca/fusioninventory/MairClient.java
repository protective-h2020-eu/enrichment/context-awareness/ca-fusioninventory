package eu.h2020.protective.ca.fusioninventory;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.http.HttpEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import eu.h2020.protective.ca.mair.api.model.core.Graph;
import lombok.extern.slf4j.XSlf4j;

@Service
@XSlf4j
public class MairClient {

	@Autowired
	private ConfigurationService config;

    private final RestTemplate restTemplate;

    public MairClient(RestTemplateBuilder restTemplateBuilder) { // FIXME improve auth.
        this.restTemplate = restTemplateBuilder.basicAuthentication("user", "password").build();
    }

    public void graphAdd(Graph graph) {
		String requestUrl = getRequestUrl("graph-add");
		HttpEntity<Graph> requestBody = new HttpEntity<>(graph);
		restTemplate.postForLocation(requestUrl, requestBody);
	}
	
	public void graphClear() {
		String requestUrl = getRequestUrl("graph-clear");
		restTemplate.postForLocation(requestUrl, null);
	}
	
	private String getRequestUrl(String operation) {
		String requestUrl = config.getCaMairUrl() + "/api/" + operation;
		log.info("requestUrl = '{}'", requestUrl);
		return requestUrl;
	}
	
}
