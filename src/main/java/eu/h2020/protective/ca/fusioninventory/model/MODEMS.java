package eu.h2020.protective.ca.fusioninventory.model;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper=true)
public class MODEMS extends FusionInventory {

	@ApiModelProperty(required = false, value = "")
	private String DESCRIPTION;

	@ApiModelProperty(required = false, value = "")
	private String NAME;

	@ApiModelProperty(required = false, value = "")
	private String TYPE;

	@ApiModelProperty(required = false, value = "")
	private String MODEL;

}
