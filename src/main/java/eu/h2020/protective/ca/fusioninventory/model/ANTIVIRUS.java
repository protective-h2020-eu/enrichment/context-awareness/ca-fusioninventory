package eu.h2020.protective.ca.fusioninventory.model;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper=true)
public class ANTIVIRUS extends FusionInventory {

    @ApiModelProperty(required = false, value = "")
	private String COMPANY;

	@ApiModelProperty(required = false, value = "")
	private String ENABLED;

	@ApiModelProperty(required = false, value = "")
	private String GUID;

	@ApiModelProperty(required = false, value = "")
	private String NAME;

	@ApiModelProperty(required = false, value = "")
	private String UPTODATE;

	@ApiModelProperty(required = false, value = "")
	private String VERSION;

	@ApiModelProperty(required = false, value = "")
	private String DATFILECREATION;

	@ApiModelProperty(required = false, value = "")
	private String DATFILEVERSION;

	@ApiModelProperty(required = false, value = "")
	private String ENGINEVERSION32;

	@ApiModelProperty(required = false, value = "")
	private String ENGINEVERSION64;

}
