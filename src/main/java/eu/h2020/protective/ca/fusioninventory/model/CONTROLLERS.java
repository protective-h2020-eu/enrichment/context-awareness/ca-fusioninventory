package eu.h2020.protective.ca.fusioninventory.model;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper=true)
public class CONTROLLERS extends FusionInventory {

	@ApiModelProperty(required = false, value = "")
	private String CAPTION;
	
	@ApiModelProperty(required = false, value = "")
	private String DRIVER;

	@ApiModelProperty(required = false, value = "")
	private String NAME;

	@ApiModelProperty(required = false, value = "")
	private String MANUFACTURER;

	@ApiModelProperty(required = false, value = "")
	private String PCICLASS;

	@ApiModelProperty(required = false, value = "")
	private String VENDORID;

	@ApiModelProperty(required = false, value = "")
	private String PRODUCTID;

	@ApiModelProperty(required = false, value = "")
	private String PCISUBSYSTEMID;

	@ApiModelProperty(required = false, value = "")
	private String PCISLOT;

	@ApiModelProperty(required = false, value = "")
	private String TYPE;

	@ApiModelProperty(required = false, value = "")
	private String REV;

}
