package eu.h2020.protective.ca.fusioninventory.filter;

import java.io.IOException;
import java.io.InputStream;

import javax.servlet.ReadListener;
import javax.servlet.ServletInputStream;

public class DecompressZlibServletInputStream extends ServletInputStream {
	private InputStream inputStream;

	public DecompressZlibServletInputStream(InputStream input) {
		inputStream = input;
	}

	@Override
	public int read() throws IOException {
		return inputStream.read();
	}

	@Override
	public boolean isFinished() {
		return false;
	}

	@Override
	public boolean isReady() {
		return true;
	}

	@Override
	public void setReadListener(ReadListener listener) {
	}

}