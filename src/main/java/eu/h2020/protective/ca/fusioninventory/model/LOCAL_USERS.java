package eu.h2020.protective.ca.fusioninventory.model;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper=true)
public class LOCAL_USERS extends FusionInventory {

	@ApiModelProperty(required = false, value = "")
	private String HOME;

	@ApiModelProperty(required = false, value = "")
	private String ID;

	@ApiModelProperty(required = false, value = "")
	private String LOGIN;

	@ApiModelProperty(required = false, value = "")
	private String NAME;

	@ApiModelProperty(required = false, value = "")
	private String SHELL;

}
