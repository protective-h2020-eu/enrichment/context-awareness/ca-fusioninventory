package eu.h2020.protective.ca.fusioninventory.model;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper=true)
public class MONITORS extends FusionInventory {

	@ApiModelProperty(required = false, value = "")
	private String BASE64;

	@ApiModelProperty(required = false, value = "")
	private String CAPTION;
	
	@ApiModelProperty(required = false, value = "")
	private String DESCRIPTION;

	@ApiModelProperty(required = false, value = "")
	private String MANUFACTURER;
	
	@ApiModelProperty(required = false, value = "")
	private String SERIAL;

	@ApiModelProperty(required = false, value = "")
	private String UUENCODE;
	
	@ApiModelProperty(required = false, value = "")
	private String NAME;

	@ApiModelProperty(required = false, value = "")
	private String TYPE;
	
	@ApiModelProperty(required = false, value = "")
	private String ALTSERIAL;

	@ApiModelProperty(required = false, value = "")
	private String PORT;
	
}
