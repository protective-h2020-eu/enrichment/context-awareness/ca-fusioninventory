package eu.h2020.protective.ca.fusioninventory.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper=true)
@ApiModel
public class SOFTWARES extends FusionInventory {

	@ApiModelProperty(required = false, value = "Ignore")
	private String COMMENTS;

	@ApiModelProperty(required = false, value = "Ignore")
	private String FILESIZE;

	@ApiModelProperty(required = false, value = "Ignore")
	private String FOLDER;

	@ApiModelProperty(required = false, value = "Ignore")
	private String FROM;

	@ApiModelProperty(required = false, value = "Ignore")
	private String HELPLINK;

	@ApiModelProperty(required = false, value = "Ignore")
	private String INSTALLDATE;

	@ApiModelProperty(required = false, value = "Maps to Application.role")
	private String NAME;

	@ApiModelProperty(required = false, value = "Ignore")
	private String NO_REMOVE;

	@ApiModelProperty(required = false, value = "Ignore")
	private String RELEASE_TYPE;

	@ApiModelProperty(required = false, value = "Ignore")
	private String PUBLISHER;

	@ApiModelProperty(required = false, value = "Ignore")
	private String UNINSTALL_STRING;

	@ApiModelProperty(required = false, value = "Ignore")
	private String URL_INFO_ABOUT;

	@ApiModelProperty(required = false, value = "Maps to Application.version")
	private String VERSION;

	@ApiModelProperty(required = false, value = "Ignore")
	private String VERSION_MINOR;

	@ApiModelProperty(required = false, value = "Ignore")
	private String VERSION_MAJOR;

	@ApiModelProperty(required = false, value = "Ignore")
	private String GUID;

	@ApiModelProperty(required = false, value = "Maps to Application.targetArchitecture")
	private String ARCH;

	@ApiModelProperty(required = false, value = "Ignore")
	private String USERNAME;

	@ApiModelProperty(required = false, value = "Ignore")
	private String USERID;

	@ApiModelProperty(required = false, value = "Ignore")
	private String SYSTEM_CATEGORY;

}
