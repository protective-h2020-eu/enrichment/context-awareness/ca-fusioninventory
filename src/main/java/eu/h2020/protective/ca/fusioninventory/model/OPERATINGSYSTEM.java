package eu.h2020.protective.ca.fusioninventory.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper=true)
@ApiModel
public class OPERATINGSYSTEM extends FusionInventory {

	@ApiModelProperty(required = false, value = "Maps to OperatingSystem.kernelName")
	private String KERNEL_NAME;

	@ApiModelProperty(required = false, value = "Maps to OperatingSystem.kernelVersion")
	private String KERNEL_VERSION;

	@ApiModelProperty(required = false, value = "Maps to OperatingSystem.role")
	private String NAME;

	@ApiModelProperty(required = false, value = "Maps to OperatingSystem.version")
	private String VERSION;

	@ApiModelProperty(required = false, value = "Maps to OperatingSystem.fullName")
	private String FULL_NAME;

	@ApiModelProperty(required = false, value = "Ignore")
	private String SERVICE_PACK;

	@ApiModelProperty(required = false, value = "Ignore")
	private String INSTALL_DATE;

	@ApiModelProperty(required = false, value = "Ignore")
	private String FQDN;

	@ApiModelProperty(required = false, value = "Ignore")
	private String DNS_DOMAIN;

	@ApiModelProperty(required = false, value = "Ignore")
	private String HOSTID;

	@ApiModelProperty(required = false, value = "Ignore")
	private String SSH_KEY;

	@ApiModelProperty(required = false, value = "Maps to OperatingSystem.targetArchitecture")
	private String ARCH;

	@ApiModelProperty(required = false, value = "Ignore")
	private String BOOT_TIME;

	@ApiModelProperty(required = false, value = "Ignore")
	private TIMEZONE TIMEZONE;

}
