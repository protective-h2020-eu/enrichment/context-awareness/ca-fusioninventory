package eu.h2020.protective.ca.fusioninventory.model;

import com.fasterxml.jackson.databind.annotation.JsonNaming;

import eu.h2020.protective.ca.fusioninventory.xml.SameCaseNamingStrategy;

@JsonNaming(SameCaseNamingStrategy.class)
public class FusionInventory {
	// Model matches Agent 2.3.20
}
