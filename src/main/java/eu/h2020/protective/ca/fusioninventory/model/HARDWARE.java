package eu.h2020.protective.ca.fusioninventory.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper=true)
@ApiModel
public class HARDWARE extends FusionInventory {

	@ApiModelProperty(required = false, value = "Maps to OperatingSystem.User.name")
	private String USERID;

	@ApiModelProperty(required = false, value = "Maps to OperatingSystem.version")
	private String OSVERSION;

	@ApiModelProperty(required = false, value = "Ignore")
	private String PROCESSORN;

	@ApiModelProperty(required = false, value = "Maps to OperatingSystem.comment")
	private String OSCOMMENTS;

	@ApiModelProperty(required = false, value = "Ignore")
	private String CHECKSUM;

	@ApiModelProperty(required = false, value = "Ignore")
	private String PROCESSORT;

	@ApiModelProperty(required = false, value = "Maps to Computer.name")
	private String NAME;

	@ApiModelProperty(required = false, value = "Ignore")
	private String PROCESSORS;

	@ApiModelProperty(required = false, value = "Ignore")
	private String SWAP;

	@ApiModelProperty(required = false, value = "Ignore")
	private String ETIME;

	@ApiModelProperty(required = false, value = "Ignore")
	private String TYPE;

	@ApiModelProperty(required = false, value = "Maps to OperatingSystem.name")
	private String OSNAME;

	@ApiModelProperty(required = false, value = "Maps to Computer.ipAddress")
	private String IPADDR;

	@ApiModelProperty(required = false, value = "Ignore")
	private String WORKGROUP;

	@ApiModelProperty(required = false, value = "Ignore")
	private String DESCRIPTION;

	@ApiModelProperty(required = false, value = "Ignore")
	private String MEMORY;

	@ApiModelProperty(required = false, value = "Maps to Computer.key")
	private String UUID;

	@ApiModelProperty(required = false, value = "Maps to Computer.dns")
	private String DNS;

	@ApiModelProperty(required = false, value = "Ignore")
	private String LASTLOGGEDUSER;

	@ApiModelProperty(required = false, value = "Ignore")
	private String USERDOMAIN;

	@ApiModelProperty(required = false, value = "Ignore")
	private String DATELASTLOGGEDUSER;

	@ApiModelProperty(required = false, value = "Ignore")
	private String DEFAULTGATEWAY;

	@ApiModelProperty(required = false, value = "Ignore: e.g. 'QEMU'")
	private String VMSYSTEM;

	@ApiModelProperty(required = false, value = "Ignore")
	private String WINOWNER;

	@ApiModelProperty(required = false, value = "Ignore")
	private String WINPRODID;

	@ApiModelProperty(required = false, value = "Ignore")
	private String WINPRODKEY;

	@ApiModelProperty(required = false, value = "Ignore")
	private String WINCOMPANY;

	@ApiModelProperty(required = false, value = "Ignore")
	private String WINLANG;
	
	@ApiModelProperty(required = false, value = "Ignore")
	private String CHASSIS_TYPE;

	@ApiModelProperty(required = false, value = "Ignore")
	private String VMNAME;
	
	@ApiModelProperty(required = false, value = "Ignore")
	private String VMHOSTSERIAL;
	
	// in different place in Agent to previous properties in Agent
	@ApiModelProperty(required = false, value = "Maps to Computer.architecture")
	private String ARCHNAME;
    
}
