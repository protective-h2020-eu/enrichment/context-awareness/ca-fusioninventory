package eu.h2020.protective.ca.fusioninventory.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper=true)
@ApiModel
public class VIRTUALMACHINES extends FusionInventory {

	@ApiModelProperty(required = false, value = "Ignore")
    private String MEMORY;

	@ApiModelProperty(required = false, value = "Ignore")
	private String NAME;

	@ApiModelProperty(required = false, value = "Ignore")
    private String UUID;

	@ApiModelProperty(required = false, value = "Ignore")
	private String STATUS;

	@ApiModelProperty(required = false, value = "Ignore")
    private String SUBSYSTEM;

	@ApiModelProperty(required = false, value = "Ignore")
	private String VMTYPE;

	@ApiModelProperty(required = false, value = "Ignore")
    private String VCPU;

	@ApiModelProperty(required = false, value = "Ignore")
	private String MAC;

	@ApiModelProperty(required = false, value = "Ignore")
    private String COMMENT;

	@ApiModelProperty(required = false, value = "Ignore")
	private String OWNER;

	@ApiModelProperty(required = false, value = "Ignore")
    private String SERIAL;

	@ApiModelProperty(required = false, value = "Ignore")
	private String IMAGE;

}
