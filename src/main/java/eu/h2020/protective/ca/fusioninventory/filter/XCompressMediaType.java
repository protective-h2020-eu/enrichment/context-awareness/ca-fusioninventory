package eu.h2020.protective.ca.fusioninventory.filter;

import java.nio.charset.StandardCharsets;

import org.springframework.http.MediaType;

public class XCompressMediaType extends MediaType {

	private static final long serialVersionUID = 418580985214920909L;

	private static final String TYPE_VALUE = "Application"; // XXX 'A' to match injector
	private static final String SUBTYPE_VALUE = "x-compress";
	
	public static final MediaType APPLICATION_X_COMPRESS = new XCompressMediaType();
	public static final String APPLICATION_X_COMPRESS_VALUE = TYPE_VALUE + "/" + SUBTYPE_VALUE;
		
	private XCompressMediaType() {
		super(TYPE_VALUE, SUBTYPE_VALUE, StandardCharsets.UTF_8);
	}

}
