package eu.h2020.protective.ca.fusioninventory.model;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper=true)
public class LICENSEINFOS extends FusionInventory {

	@ApiModelProperty(required = false, value = "")
	private String NAME;

	@ApiModelProperty(required = false, value = "")
	private String FULLNAME;

	@ApiModelProperty(required = false, value = "")
	private String KEY;

	@ApiModelProperty(required = false, value = "")
	private String COMPONENTS;

	@ApiModelProperty(required = false, value = "")
	private String TRIAL;

	@ApiModelProperty(required = false, value = "")
	private String UPDATE;

	@ApiModelProperty(required = false, value = "")
	private String OEM;

	@ApiModelProperty(required = false, value = "")
	private String ACTIVATION_DATE;

	@ApiModelProperty(required = false, value = "")
	private String PRODUCTID;

}
