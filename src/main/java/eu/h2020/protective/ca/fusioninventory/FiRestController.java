package eu.h2020.protective.ca.fusioninventory;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.info.BuildProperties;
import org.springframework.http.HttpEntity;
import org.springframework.http.MediaType;
import org.springframework.http.converter.xml.MappingJackson2XmlHttpMessageConverter;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import eu.h2020.protective.ca.fusioninventory.filter.XCompressMediaType;
import eu.h2020.protective.ca.fusioninventory.filter.XCompressZlibMediaType;
import eu.h2020.protective.ca.fusioninventory.model.REPLY;
import eu.h2020.protective.ca.fusioninventory.model.REQUEST;
import eu.h2020.protective.ca.mair.api.model.core.Graph;

@RequestMapping("/api")
@RestController
public class FiRestController {

	@Autowired
	private Optional<BuildProperties> buildProperties;

	@Autowired
	private MairClient mairClient;

	@Autowired
	private SyncMair syncMair;

	@Autowired
	private MappingJackson2XmlHttpMessageConverter xmlConverter;

	@PostConstruct
	private void init() {
		List<MediaType> oldMediaTypes = xmlConverter.getSupportedMediaTypes();
		List<MediaType> newMediaTypes = new ArrayList<>(oldMediaTypes.size() + 1);
		newMediaTypes.addAll(oldMediaTypes);
		newMediaTypes.add(XCompressZlibMediaType.APPLICATION_X_COMPRESS_ZLIB);
		xmlConverter.setSupportedMediaTypes(newMediaTypes);
	}

	@GetMapping(value = "/about", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public AboutProperties about() {
		return new AboutProperties(buildProperties);
	}

	@RequestMapping(value = "/upload", method = { RequestMethod.GET,
			RequestMethod.POST }, consumes = {
					MediaType.APPLICATION_XML_VALUE,
					XCompressZlibMediaType.APPLICATION_X_COMPRESS_ZLIB_VALUE,
					XCompressMediaType.APPLICATION_X_COMPRESS_VALUE }, produces = {
							XCompressZlibMediaType.APPLICATION_X_COMPRESS_ZLIB_VALUE })
	public REPLY uploadOCSxml(HttpEntity<String> httpEntity) throws Exception {
		String xmlString = httpEntity.getBody();
		REQUEST request = syncMair.getRequestFromXml(xmlString);
		REPLY reply = new REPLY();
		if (syncMair.isPrologXml(request)) {
			reply.setRESPONSE("SEND");
			reply.setPROLOG_FREQ(12); // Tell agent to call back in 1 hour.
			return reply;
		} else if (syncMair.isInventoryXml(request)) {
			Graph graph = syncMair.getGraphFromDeviceXml(request);
			mairClient.graphAdd(graph);
			reply.setRESPONSE("SEND"); // FIXME what to reply
			reply.setPROLOG_FREQ(12); // Tell agent to call back in 2 hours.
			return reply;
		} else {
			reply.setRESPONSE("SEND"); // FIXME what to reply
			reply.setPROLOG_FREQ(12); // Tell agent to call back in 3 hours.
			return reply;
		}
	}

}