package eu.h2020.protective.ca.fusioninventory.model;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper=true)
public class CONTENT extends FusionInventory {

	@ApiModelProperty(required = false, value = "")
	private BIOS BIOS;

	@ApiModelProperty(required = false, value = "")
	private HARDWARE HARDWARE;

	@ApiModelProperty(required = false, value = "")
	private OPERATINGSYSTEM OPERATINGSYSTEM;

	@ApiModelProperty(required = false, value = "")
	private ACCESSLOG ACCESSLOG;

	@ApiModelProperty(required = false, value = "")
	private ANTIVIRUS[] ANTIVIRUS = {};

	@ApiModelProperty(required = false, value = "")
	private BATTERIES[] BATTERIES = {};

	@ApiModelProperty(required = false, value = "")
	private CONTROLLERS[] CONTROLLERS = {};

	@ApiModelProperty(required = false, value = "")
	private CPUS[] CPUS = {};

	@ApiModelProperty(required = false, value = "")
	private DRIVES[] DRIVES = {};

	@ApiModelProperty(required = false, value = "")
	private ENVS[] ENVS = {};

	@ApiModelProperty(required = false, value = "")
	private INPUTS[] INPUTS = {};

	@ApiModelProperty(required = false, value = "")
	private FIREWALL FIREWALL;

	@ApiModelProperty(required = false, value = "")
	private LICENSEINFOS[] LICENSEINFOS = {};

	@ApiModelProperty(required = false, value = "")
	private LOCAL_GROUPS[] LOCAL_GROUPS = {};

	@ApiModelProperty(required = false, value = "")
	private LOCAL_USERS[] LOCAL_USERS = {};

	@ApiModelProperty(required = false, value = "")
	private LOGICAL_VOLUMES[] LOGICAL_VOLUMES = {};

	@ApiModelProperty(required = false, value = "")
	private MEMORIES[] MEMORIES = {};

	@ApiModelProperty(required = false, value = "")
	private MODEMS[] MODEMS = {};

	@ApiModelProperty(required = false, value = "")
	private MONITORS[] MONITORS = {};

	@ApiModelProperty(required = false, value = "")
	private NETWORKS[] NETWORKS = {};

	@ApiModelProperty(required = false, value = "")
	private PHYSICAL_VOLUMES[] PHYSICAL_VOLUMES = {};

	@ApiModelProperty(required = false, value = "")
	private PORTS[] PORTS = {};

	@ApiModelProperty(required = false, value = "")
	private PRINTERS[] PRINTERS = {};

	@ApiModelProperty(required = false, value = "")
	private PROCESSES[] PROCESSES = {};

	@ApiModelProperty(required = false, value = "")
	private REGISTRY REGISTRY;

	@ApiModelProperty(required = false, value = "")
	private REMOTE_MGMT REMOTE_MGMT;

	@ApiModelProperty(required = false, value = "")
	private RUDDER RUDDER;

	@ApiModelProperty(required = false, value = "")
	private SLOTS[] SLOTS = {};

	@ApiModelProperty(required = false, value = "")
	private SOFTWARES[] SOFTWARES = {};

	@ApiModelProperty(required = false, value = "")
	private SOUNDS[] SOUNDS = {};

	@ApiModelProperty(required = false, value = "")
	private STORAGES[] STORAGES = {};

	@ApiModelProperty(required = false, value = "")
	private VIDEOS[] VIDEOS = {};

	@ApiModelProperty(required = false, value = "")
	private USBDEVICES[] USBDEVICES = {};

	@ApiModelProperty(required = false, value = "")
	private USERS[] USERS = {};

	@ApiModelProperty(required = false, value = "")
	private VIRTUALMACHINES[] VIRTUALMACHINES = {};

	@ApiModelProperty(required = false, value = "")
	private VOLUME_GROUPS[] VOLUME_GROUPS = {};

	@ApiModelProperty(required = false, value = "")
	private VERSIONPROVIDER VERSIONPROVIDER;

	// in different place in Agent to previous properties in Agent
	@ApiModelProperty(required = false, value = "")
	private String VERSIONCLIENT; 

}
