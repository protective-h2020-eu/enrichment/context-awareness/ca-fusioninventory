package eu.h2020.protective.ca.fusioninventory.model;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper=true)
public class DRIVES extends FusionInventory {

	@ApiModelProperty(required = false, value = "")
    private String CREATEDATE;

	@ApiModelProperty(required = false, value = "")
    private String DESCRIPTION;

	@ApiModelProperty(required = false, value = "")
    private String FREE;

	@ApiModelProperty(required = false, value = "")
    private String FILESYSTEM;

	@ApiModelProperty(required = false, value = "")
    private String LABEL;

	@ApiModelProperty(required = false, value = "")
    private String LETTER;

	@ApiModelProperty(required = false, value = "")
    private String SERIAL;

	@ApiModelProperty(required = false, value = "")
    private String SYSTEMDRIVE;

    @ApiModelProperty(required = false, value = "")
    private String TOTAL;

	@ApiModelProperty(required = false, value = "")
    private String TYPE;

	@ApiModelProperty(required = false, value = "")
    private String VOLUMN;

}
