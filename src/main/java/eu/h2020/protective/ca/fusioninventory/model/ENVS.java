package eu.h2020.protective.ca.fusioninventory.model;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper=true)
public class ENVS extends FusionInventory {

	@ApiModelProperty(required = false, value = "")
	private String KEY;

	@ApiModelProperty(required = false, value = "")
	private String VAL;

}
