package eu.h2020.protective.ca.fusioninventory.model;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper=true)
public class PRINTERS extends FusionInventory {

	@ApiModelProperty(required = false, value = "")
	private String COMMENT;

	@ApiModelProperty(required = false, value = "")
	private String DESCRIPTION;

	@ApiModelProperty(required = false, value = "")
	private String DRIVER;

	@ApiModelProperty(required = false, value = "")
	private String NAME;

	@ApiModelProperty(required = false, value = "")
	private String NETWORK;

	@ApiModelProperty(required = false, value = "")
	private String PORT;

	@ApiModelProperty(required = false, value = "")
	private String RESOLUTION;

	@ApiModelProperty(required = false, value = "")
	private String SHARED;

	@ApiModelProperty(required = false, value = "")
	private String STATUS;

	@ApiModelProperty(required = false, value = "")
	private String ERRSTATUS;

	@ApiModelProperty(required = false, value = "")
	private String SERVERNAME;

	@ApiModelProperty(required = false, value = "")
	private String SHARENAME;

	@ApiModelProperty(required = false, value = "")
	private String PRINTPROCESSOR;

	@ApiModelProperty(required = false, value = "")
	private String SERIAL;

}
