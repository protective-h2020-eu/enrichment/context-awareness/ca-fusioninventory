package eu.h2020.protective.ca.fusioninventory.model;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper=true)
public class PROCESSES extends FusionInventory {

	@ApiModelProperty(required = false, value = "")
	private String USER;

	@ApiModelProperty(required = false, value = "")
	private String PID;

	@ApiModelProperty(required = false, value = "")
	private String CPUUSAGE;

	@ApiModelProperty(required = false, value = "")
	private String MEM;

	@ApiModelProperty(required = false, value = "")
	private String VIRTUALMEMORY;

	@ApiModelProperty(required = false, value = "")
	private String TTY;

	@ApiModelProperty(required = false, value = "")
	private String STARTED;

    @ApiModelProperty(required = false, value = "")
	private String CMD;

}
