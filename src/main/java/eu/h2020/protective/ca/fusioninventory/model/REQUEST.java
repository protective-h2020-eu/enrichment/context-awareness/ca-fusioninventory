package eu.h2020.protective.ca.fusioninventory.model;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper=true)
public class REQUEST extends FusionInventory {

	@ApiModelProperty(required = false, value = "")
	private String DEVICEID;

	@ApiModelProperty(required = false, value = "")
	private String QUERY;

	@ApiModelProperty(required = false, value = "")
	private String TOKEN;

	@ApiModelProperty(required = false, value = "")
	private CONTENT CONTENT;

}
