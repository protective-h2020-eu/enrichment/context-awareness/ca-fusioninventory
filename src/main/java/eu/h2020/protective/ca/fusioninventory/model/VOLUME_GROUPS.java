package eu.h2020.protective.ca.fusioninventory.model;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper=true)
public class VOLUME_GROUPS extends FusionInventory {

	@ApiModelProperty(required = false, value = "")
	private String VG_NAME;

	@ApiModelProperty(required = false, value = "")
	private String PV_COUNT;

	@ApiModelProperty(required = false, value = "")
	private String LV_COUNT;

	@ApiModelProperty(required = false, value = "")
	private String ATTR;

	@ApiModelProperty(required = false, value = "")
	private String SIZE;

	@ApiModelProperty(required = false, value = "")
	private String FREE;

	@ApiModelProperty(required = false, value = "")
	private String VG_UUID;

	@ApiModelProperty(required = false, value = "")
	private String VG_EXTENT_SIZE;

}
