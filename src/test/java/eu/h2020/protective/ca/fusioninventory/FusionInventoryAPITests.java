package eu.h2020.protective.ca.fusioninventory;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.core.io.Resource;
import org.springframework.http.MediaType;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;
import static io.restassured.RestAssured.given;

import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.util.Scanner;

@RunWith(SpringRunner.class)
@TestPropertySource(locations = "classpath:application-test.properties")
@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
//@ExtendWith(SpringExtension.class)
public class FusionInventoryAPITests {
	
	@LocalServerPort
	private int port;

	@Value("classpath:inventory.xml")
	private Resource inventoryXml;
	
	@Value("classpath:VMWareHostAndVM.xml")
	private Resource vmwareXml;
	
	@Test
	public void getResponseFromXmlUpload() throws Exception {
		
		InputStream inputStream = inventoryXml.getInputStream();
		String xmlString = null;
		try (Scanner scanner = new Scanner(inputStream, StandardCharsets.UTF_8.name())) {
	        xmlString = scanner.useDelimiter("\\A").next();
	    }
				
		given()
		.port(port)
        .contentType(MediaType.APPLICATION_XML_VALUE)
        .body(xmlString)
        .when()
        .post("api/upload")
        .then()
        .statusCode(200);
	}
	@Test
	public void getResponseFromVmwareXmlUpload() throws Exception {
		
		InputStream inputStream = vmwareXml.getInputStream();
		String xmlString = null;
		try (Scanner scanner = new Scanner(inputStream, StandardCharsets.UTF_8.name())) {
	        xmlString = scanner.useDelimiter("\\A").next();
	    }
				
		given()
		.port(port)
        .contentType(MediaType.APPLICATION_XML_VALUE)
        .body(xmlString)
        .when()
        .post("api/upload")
        .then()
        .statusCode(200);
	}
}
