package eu.h2020.protective.ca.fusioninventory;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;

import eu.h2020.protective.ca.fusioninventory.model.REQUEST;
import eu.h2020.protective.ca.mair.api.model.core.Graph;

@RunWith(SpringRunner.class)
@SpringBootTest
@TestPropertySource(locations = "classpath:application-test.properties")
public class FusionInventoryFunctionalTests {
	
	@Autowired
	private SyncMair syncMair;

	@Test
	public void getRequestFromDeviceXml() throws Exception {
		REQUEST request = syncMair.getRequestFromXml();
		assertThat(request.getDEVICEID()).isNull();
		assertThat(request.getQUERY()).isEqualTo("INVENTORY");
	}

	@Test
	public void getGraphFromDeviceXml() throws Exception {
		REQUEST request = syncMair.getRequestFromXml();
		Graph graph = syncMair.getGraphFromDeviceXml(request);
		assertThat(graph).isNotNull();
	}

}
