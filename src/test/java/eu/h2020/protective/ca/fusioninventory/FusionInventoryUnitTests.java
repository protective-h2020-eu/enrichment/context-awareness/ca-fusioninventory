package eu.h2020.protective.ca.fusioninventory;

import static org.assertj.core.api.Assertions.assertThat;

import java.io.InputStream;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.fasterxml.jackson.dataformat.xml.JacksonXmlModule;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;

import eu.h2020.protective.ca.fusioninventory.model.REQUEST;

public class FusionInventoryUnitTests {

	private InputStream xmlStream = null;

	@Before()
	public void beforeEach() throws Exception {
		ClassLoader classLoader = getClass().getClassLoader();
		xmlStream = classLoader.getResource("inventory.xml").openStream();
	}

	@After()
	public void afterEach() throws Exception {
		xmlStream.close();
	}

	@Test
	public void readInventoryFromXml() throws Exception {
		JacksonXmlModule module = new JacksonXmlModule();
		module.setDefaultUseWrapper(false);
		XmlMapper xmlMapper = new XmlMapper(module);
		REQUEST readRequest = xmlMapper.readValue(xmlStream, REQUEST.class);
		assertThat(readRequest.getDEVICEID()).isNull();
		assertThat(readRequest.getQUERY()).isEqualTo("INVENTORY");
		assertThat(readRequest.getCONTENT()).isNotNull();
		assertThat(readRequest.getCONTENT().getBIOS()).isNotNull();
		assertThat(readRequest.getCONTENT().getBIOS().getSSN())
				.isEqualTo("4c4c4544-005a-4810-8034-c3c04f344a32");
	}

}
